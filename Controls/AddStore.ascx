﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStore.ascx.cs" Inherits="Controls_AddStore" %>
<%@ Register Src="~/Controls/BaseAddController.ascx" TagPrefix="uc1" TagName="BaseAddController" %>


<uc1:BaseAddController runat="server" id="BaseAddController">
    <HeaderTemplate><h3>Add A Store</h3></HeaderTemplate>
    <BodyTemplate>
        <asp:TextBox runat="server" required="true" placeholder="Store Name" CssClass="input-group-lg"></asp:TextBox>
        
    </BodyTemplate>
    <FooterTemplate>
<asp:Button runat="server" Text="Save!" ID="SubmitStore" CssClass="btn-group-lg btn-success" OnClick="SubmitStore_Click"/>
        
    </FooterTemplate>
</uc1:BaseAddController>
