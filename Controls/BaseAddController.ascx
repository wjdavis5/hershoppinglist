﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BaseAddController.ascx.cs" Inherits="MyControls.BaseAddControl" %>
<link href="../Content/bootstrap-theme.css" rel="stylesheet" />
<link href="../Content/bootstrap.css" rel="stylesheet" />
<div class="panel panel-primary">
    <div class="panel-heading"><asp:PlaceHolder ID="PanelHeader" runat="server"></asp:PlaceHolder></div>
    <div class="panel-body">
        <asp:PlaceHolder ID="PanelBody" runat="server"></asp:PlaceHolder>
     </div>
    <div class="panel-footer"><asp:PlaceHolder ID="PanelFooter" runat="server"></asp:PlaceHolder></div>
</div>