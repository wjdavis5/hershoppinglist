﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HerShoppingList.Models;

namespace HerShoppingList.Controllers
{
    public class JsonController : Controller
    {
        //
        // GET: /StoreList/
        public JsonResult GetUniqueStores()
        {
            var returnList = new List<string>();
            using (var conn = MyConfig.SqlConnection())
            {
                conn.Open();
                using (var cmd = new SqlCommand("GetUniqueStores", conn) { CommandType = CommandType.StoredProcedure })
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            returnList.Add(reader.GetString(0));
                        }
                    }
                }

            }
            return this.Json(returnList,JsonRequestBehavior.AllowGet);
        }
	}
}