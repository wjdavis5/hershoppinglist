﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HerShoppingList.Models;

namespace HerShoppingList.Controllers
{
    public class ReceiptController : Controller
    {
        //
        // GET: /Receipt/
        private Receipt _receipt;
        public ActionResult Index()
        {
            if (_receipt == null)
                _receipt = new Receipt();
            Session.Add("Receipt",_receipt);
            return View(_receipt);
        }

        [HttpPost]
        public ActionResult AddItem(Receipt receipt)
        {
            
            receipt.Items.Add(receipt.NewItem);
            _receipt = receipt;
            if (ModelState.IsValid)
            {

                return View("Index", _receipt);
            }
            return View("Index", _receipt);
        }
        //
        // GET: /Receipt/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Receipt/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Receipt/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Receipt/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Receipt/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Receipt/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Receipt/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
