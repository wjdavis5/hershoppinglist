﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HerShoppingList.Models;

/// <summary>
/// Summary description for MyConfig
/// </summary>
public static class MyConfig
{

    public static Configuration wBConfiguration =
           System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/HerShoppingList");

    public static SqlConnection SqlConnection()
    {
        //#if Debug
        var conStr = wBConfiguration.ConnectionStrings.ConnectionStrings["DefaultConnection"];
       // #else
       //         var conStr = wBConfiguration.ConnectionStrings.ConnectionStrings["RemoteConStr"];
       // #endif
        return new SqlConnection(conStr.ConnectionString);
    }

    
    

}