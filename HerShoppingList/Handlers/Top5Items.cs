﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Services;

[ServiceContract(Namespace = "")]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class Top5Items
{
	// To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
	// To create an operation that returns XML,
	//     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
	//     and include the following line in the operation body:
	//         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
	[OperationContract]
    [WebGet(UriTemplate = "GetSimilarItems?name={name}",
        ResponseFormat=WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json)]
   public List<string> GetSimilarItems(string name)
	{
	    if (string.IsNullOrEmpty(name)) return new List<string>();
	    var txtCmd = "Select top 5 Name From dbo.Items Where Name like '" + name + "%'";
        List<string> similarList = new List<string>();
	    using (var conn = MyConfig.SqlConnection())
	    {
	        conn.Open();
	        using (var cmd = new SqlCommand(txtCmd, conn))
	        {
	            using (var reader = cmd.ExecuteReader())
	            {
	                while (reader.Read())
	                {
	                    similarList.Add(reader.GetString(0));
	                }
	            }
	        }
	    }
	    return similarList;
	}

    [OperationContract]
    [WebGet(UriTemplate = "GetUniqueStores",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json)]
    public List<string> GetUniqueStores()
    {
        var txtCmd = "GetUniqueStores";
        List<string> uniqueStoreList = new List<string>();
        using (var conn = MyConfig.SqlConnection())
        {
            conn.Open();
            using (var cmd = new SqlCommand(txtCmd, conn){CommandType = CommandType.StoredProcedure})
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        uniqueStoreList.Add(reader.GetString(0));
                    }
                }
            }
        }
        return uniqueStoreList;
    }
}
