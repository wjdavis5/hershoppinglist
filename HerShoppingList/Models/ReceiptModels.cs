﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HerShoppingList.Models
{
    public abstract class BaseEntity
    {
         [Display(Name = "Id" )]
        public long Id { get; set; }

    }
    public class Receipt: BaseEntity
    {
        [Required(AllowEmptyStrings = false,ErrorMessage = "Please Select A Store")]
        public Store  Store { get; set; }
        public List<Item> Items { get; set; }
        public Item NewItem { get; private set; }
        [Required(AllowEmptyStrings = false,ErrorMessage = "ReceiptModel.EntryDate is required")]
        public DateTime EntryDate { get; set; }
        public Guid EntryOwner { get; set; }
        public string Name { get; set; }

        [Display(Name = "Receipt Total")]
        [DataType(DataType.Currency)]
        public double ReceiptTotal
        {
            get
            {
                return Items.Sum(item => item.ItemPrice);
            }
        }

        public Receipt()
        {
            //Items = new Items();
            Items = new List<Item>();
            EntryOwner = Guid.Empty;
            EntryDate = DateTime.Now;
            Store = new Store();
            NewItem = new Item();
            
        }

       
    }

    public class Items : BaseEntity, IEnumerable<Item>
    {
        public Item NewItem { get; set; }
        private IList<Item> _items = new List<Item>();
        public IEnumerator<Item> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public void Clear() { _items.Clear();}
        public bool Contains(Item item){return _items.Contains(item);}
        public bool Remove(Item item){return _items.Remove(item);}

        public void Add(Item item)
        {
            _items.Add(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    public class Store : BaseEntity
    {
       
        [Required(ErrorMessage = "Store Name is Required",AllowEmptyStrings = false)]
        [Display(Name = "Store Name")]
        public string Name { get; set; }

        //based on state.
        [Required(ErrorMessage = "Tax Rate is Required",AllowEmptyStrings = false)]
        [Display(Name = "Tax Rate")]
        public double TaxRate { get; set; }

        public Store()
        {
            Name = string.Empty;
        }

    }

    public class Item : BaseEntity
    {
        [Required(ErrorMessage = "Item Name is Required",AllowEmptyStrings = false)]
        [Display(Name = "Item Name")]
        
        public string Name { get; set; }

        [Required(ErrorMessage = "Unit Price is Required",AllowEmptyStrings = false)]
        [Display(Name="Unit Price")]
        [DataType(DataType.Currency)]
        public double UnitPrice { get; set; }

        [Required(ErrorMessage = "Unit is Required",AllowEmptyStrings = false)]
        public UnitType UnitType { get; set; }

        [Required(ErrorMessage = "Quantity is Required",AllowEmptyStrings = false)]
        [Display(Name="Quantity")]
        public double Quantity { get; set; }

        [Required(ErrorMessage = "Category is Required",AllowEmptyStrings = false)]
        [Display(Name="Category")]
        public Category Category { get; set; }

        [Display(Name = "Item Total")]
        [DataType(DataType.Currency)]
        public double ItemPrice{get { return (Quantity*UnitPrice); }}

        public Item()
        {
            Category = new Category();
            UnitType = UnitType.None;
            Name = string.Empty;
        }
        
    }

    public class Category:BaseEntity
    {
        [Required(ErrorMessage = "We need to know if this category is taxable")]
        [Display(Name = "Is Taxable")]
        public bool IsTaxable { get; set; }
        
        [Required(ErrorMessage = "Category Name is Required",AllowEmptyStrings = false)]
        [Display(Name = "Item Name")]
        public string Name { get; set; }

        public Category()
        {
            Name = String.Empty;
        }

    }

    public enum UnitType
    {
        None,
        Each,
        Pound
    }

}