﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HerShoppingList.Startup))]
namespace HerShoppingList
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
